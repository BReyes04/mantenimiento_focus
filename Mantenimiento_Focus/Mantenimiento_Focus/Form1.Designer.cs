﻿namespace Mantenimiento_Focus
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lablEtiqueta = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textTRebas = new System.Windows.Forms.TextBox();
            this.textAmor = new System.Windows.Forms.TextBox();
            this.textRMin = new System.Windows.Forms.TextBox();
            this.textUndE = new System.Windows.Forms.TextBox();
            this.bntActulizar = new System.Windows.Forms.Button();
            this.comboFFocus = new System.Windows.Forms.ComboBox();
            this.comboUOrigen = new System.Windows.Forms.ComboBox();
            this.comboUM = new System.Windows.Forms.ComboBox();
            this.lblDescripcionArt = new System.Windows.Forms.Label();
            this.lblFrecReabast = new System.Windows.Forms.Label();
            this.txtFrecReabast = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lablEtiqueta
            // 
            this.lablEtiqueta.AutoSize = true;
            this.lablEtiqueta.Location = new System.Drawing.Point(19, 9);
            this.lablEtiqueta.Name = "lablEtiqueta";
            this.lablEtiqueta.Size = new System.Drawing.Size(46, 17);
            this.lablEtiqueta.TabIndex = 1;
            this.lablEtiqueta.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ubicacion Origen";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Familia Focus";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tiempo Reabastecimiento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 238);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Reabastecimiento Minimo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Amortiguador";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Unidad Medicion";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 283);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Unidad Empaque";
            // 
            // textTRebas
            // 
            this.textTRebas.Location = new System.Drawing.Point(231, 103);
            this.textTRebas.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textTRebas.Name = "textTRebas";
            this.textTRebas.Size = new System.Drawing.Size(259, 22);
            this.textTRebas.TabIndex = 8;
            // 
            // textAmor
            // 
            this.textAmor.Location = new System.Drawing.Point(231, 195);
            this.textAmor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textAmor.Name = "textAmor";
            this.textAmor.Size = new System.Drawing.Size(259, 22);
            this.textAmor.TabIndex = 10;
            // 
            // textRMin
            // 
            this.textRMin.Location = new System.Drawing.Point(231, 235);
            this.textRMin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textRMin.Name = "textRMin";
            this.textRMin.Size = new System.Drawing.Size(259, 22);
            this.textRMin.TabIndex = 11;
            // 
            // textUndE
            // 
            this.textUndE.Location = new System.Drawing.Point(231, 278);
            this.textUndE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textUndE.Name = "textUndE";
            this.textUndE.Size = new System.Drawing.Size(259, 22);
            this.textUndE.TabIndex = 12;
            // 
            // bntActulizar
            // 
            this.bntActulizar.Location = new System.Drawing.Point(382, 366);
            this.bntActulizar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bntActulizar.Name = "bntActulizar";
            this.bntActulizar.Size = new System.Drawing.Size(108, 27);
            this.bntActulizar.TabIndex = 13;
            this.bntActulizar.Text = "Actualizar";
            this.bntActulizar.UseVisualStyleBackColor = true;
            this.bntActulizar.Click += new System.EventHandler(this.BntActulizar_Click);
            // 
            // comboFFocus
            // 
            this.comboFFocus.FormattingEnabled = true;
            this.comboFFocus.Items.AddRange(new object[] {
            "REPUESTOS",
            "CONSCONTROLADOS",
            "CONSNOCONTROLADOS",
            "PTO"});
            this.comboFFocus.Location = new System.Drawing.Point(231, 42);
            this.comboFFocus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboFFocus.Name = "comboFFocus";
            this.comboFFocus.Size = new System.Drawing.Size(259, 24);
            this.comboFFocus.TabIndex = 14;
            // 
            // comboUOrigen
            // 
            this.comboUOrigen.FormattingEnabled = true;
            this.comboUOrigen.Location = new System.Drawing.Point(231, 74);
            this.comboUOrigen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboUOrigen.Name = "comboUOrigen";
            this.comboUOrigen.Size = new System.Drawing.Size(259, 24);
            this.comboUOrigen.TabIndex = 15;
            // 
            // comboUM
            // 
            this.comboUM.FormattingEnabled = true;
            this.comboUM.Location = new System.Drawing.Point(231, 147);
            this.comboUM.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboUM.Name = "comboUM";
            this.comboUM.Size = new System.Drawing.Size(259, 24);
            this.comboUM.TabIndex = 16;
            // 
            // lblDescripcionArt
            // 
            this.lblDescripcionArt.AutoSize = true;
            this.lblDescripcionArt.Location = new System.Drawing.Point(145, 9);
            this.lblDescripcionArt.Name = "lblDescripcionArt";
            this.lblDescripcionArt.Size = new System.Drawing.Size(46, 17);
            this.lblDescripcionArt.TabIndex = 17;
            this.lblDescripcionArt.Text = "label1";
            // 
            // lblFrecReabast
            // 
            this.lblFrecReabast.AutoSize = true;
            this.lblFrecReabast.Location = new System.Drawing.Point(19, 321);
            this.lblFrecReabast.Name = "lblFrecReabast";
            this.lblFrecReabast.Size = new System.Drawing.Size(221, 21);
            this.lblFrecReabast.TabIndex = 18;
            this.lblFrecReabast.Text = "Frec. de Reabastecimiento";
            // 
            // txtFrecReabast
            // 
            this.txtFrecReabast.Location = new System.Drawing.Point(231, 318);
            this.txtFrecReabast.Name = "txtFrecReabast";
            this.txtFrecReabast.Size = new System.Drawing.Size(259, 22);
            this.txtFrecReabast.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 426);
            this.Controls.Add(this.txtFrecReabast);
            this.Controls.Add(this.lblFrecReabast);
            this.Controls.Add(this.lblDescripcionArt);
            this.Controls.Add(this.comboUM);
            this.Controls.Add(this.comboUOrigen);
            this.Controls.Add(this.comboFFocus);
            this.Controls.Add(this.bntActulizar);
            this.Controls.Add(this.textUndE);
            this.Controls.Add(this.textRMin);
            this.Controls.Add(this.textAmor);
            this.Controls.Add(this.textTRebas);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lablEtiqueta);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Grupo Emasal - Mtto Focus";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lablEtiqueta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textTRebas;
        private System.Windows.Forms.TextBox textAmor;
        private System.Windows.Forms.TextBox textRMin;
        private System.Windows.Forms.TextBox textUndE;
        private System.Windows.Forms.Button bntActulizar;
        private System.Windows.Forms.ComboBox comboFFocus;
        private System.Windows.Forms.ComboBox comboUOrigen;
        private System.Windows.Forms.ComboBox comboUM;
        private System.Windows.Forms.Label lblDescripcionArt;
        private System.Windows.Forms.Label lblFrecReabast;
        private System.Windows.Forms.TextBox txtFrecReabast;
    }
}

