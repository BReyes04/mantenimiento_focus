﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantenimiento_Focus
{
    public partial class Form1 : Form
    {
        public static class Compania
        {
            public static string CodCompania;
        }
        public static class RowArt
        {
            public static string RowArtCod;
        }
        public static class connetionString
        {
            public static string connetionStringC;
        }
        public Form1(string Ccia, string row)
        {
            Compania.CodCompania = Ccia;
            RowArt.RowArtCod = row;
            InitializeComponent();
            connetionString.connetionStringC = "Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=cubo;Password=cubo";

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            DataTable dt2 = new DataTable();
            using (SqlConnection conn2 = new SqlConnection(connetionString.connetionStringC))
            {

                string query2 = "SELECT * FROM " + Compania.CodCompania.ToString() + ".CLASIFICACION c WHERE c.AGRUPACION = '2' ORDER BY CLASIFICACION ASC";

                SqlCommand cmd2 = new SqlCommand(query2, conn2);

                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                da2.Fill(dt2);
            }

            comboUOrigen.DisplayMember = "CLASIFICACION";
            comboUOrigen.ValueMember = "CLASIFICACION";
            comboUOrigen.DataSource = dt2;

            DataTable dt3 = new DataTable();
            using (SqlConnection conn = new SqlConnection(connetionString.connetionStringC))
            {

                string query = "SELECT * FROM " + Compania.CodCompania.ToString() + ".UNIDAD_DE_MEDIDA";

                SqlCommand cmd = new SqlCommand(query, conn);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt3);
            }

            comboUM.DisplayMember = "UNIDAD_MEDIDA";
            comboUM.ValueMember = "UNIDAD_MEDIDA";
            comboUM.DataSource = dt3;




            string[] Etiqueta = new string[1];
            string[] FAMILIA_FOCUS = new string[1];
            string[] UBICACION_ORIGEN = new string[1];
            string[] TIEMPO_REABASTECIMIENTO = new string[1];
            string[] UNIDAD_MEDICION = new string[1];
            string[] AMORTIGUADOR = new string[1];
            string[] REABASTECIMIENTO_MINIMOO = new string[1];
            string[] UNIDAD_EMPAQUE = new string[1];
            String[] DESCRIPCION_ARTICULO = new string[1];
            String[] FRECUENCIA_REABASTECIMIENTO = new string[1];


            int cont = 0;

            using (SqlConnection con = new SqlConnection(connetionString.connetionStringC))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM " + Compania.CodCompania.ToString() + ".ARTICULO WHERE RowPointer='" + RowArt.RowArtCod.ToString() + "';";
                    con.Open();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    foreach (DataRow row in dt.Rows)
                    {
                        Etiqueta[cont] = row["Articulo"].ToString();
                        FAMILIA_FOCUS[cont] = row["U_FAMILIA_FOCUS"].ToString();
                        UBICACION_ORIGEN[cont] = row["CLASIFICACION_2"].ToString();
                        TIEMPO_REABASTECIMIENTO[cont] = row["PLAZO_REABAST"].ToString();
                        UNIDAD_MEDICION[cont] = row["UNIDAD_ALMACEN"].ToString();
                        AMORTIGUADOR[cont] = row["PUNTO_DE_REORDEN"].ToString();
                        REABASTECIMIENTO_MINIMOO[cont] = row["ORDEN_MINIMA"].ToString();
                        UNIDAD_EMPAQUE[cont] = row["U_UND_EMPAQUE_FC"].ToString();
                        DESCRIPCION_ARTICULO[cont] = row["DESCRIPCION"].ToString();
                        FRECUENCIA_REABASTECIMIENTO[cont] = row["U_FREC_REABAST"].ToString();


                        cont++;
                    }

                    con.Close();
                }
                lablEtiqueta.Text = Etiqueta[0];
                comboFFocus.SelectedText = FAMILIA_FOCUS[0];
                comboUOrigen.SelectedValue = UBICACION_ORIGEN[0];
                textTRebas.Text = TIEMPO_REABASTECIMIENTO[0];
                comboUM.SelectedValue = UNIDAD_MEDICION[0];
                textAmor.Text = AMORTIGUADOR[0];
                textRMin.Text = REABASTECIMIENTO_MINIMOO[0];
                textUndE.Text = UNIDAD_EMPAQUE[0];
                lblDescripcionArt.Text = DESCRIPCION_ARTICULO[0];
                txtFrecReabast.Text = FRECUENCIA_REABASTECIMIENTO[0];

            }
        }

        private void BntActulizar_Click(object sender, EventArgs e)
{

            try
            {
                using (SqlConnection con = new SqlConnection(connetionString.connetionStringC))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string sql=null;

                        cmd.Connection = con;
                        cmd.CommandType = CommandType.Text;
                        sql= "UPDATE " + Compania.CodCompania.ToString() + ".ARTICULO SET U_FAMILIA_FOCUS='" + comboFFocus.Text.ToString() + "', CLASIFICACION_2='" + comboUOrigen.SelectedValue.ToString() + "',PLAZO_REABAST='" + textTRebas.Text.ToString() + "',UNIDAD_ALMACEN='" + comboUM.SelectedValue.ToString() + "',PUNTO_DE_REORDEN='" + textAmor.Text.ToString() + "',ORDEN_MINIMA='" + textRMin.Text.ToString() + "' ,U_UND_EMPAQUE_FC='" + textUndE.Text.ToString() + "' , U_FREC_REABAST='" + txtFrecReabast.Text.ToString() + "'  WHERE ARTICULO='" + lablEtiqueta.Text.ToString() + "'";
                        cmd.CommandText = sql;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Actualizado");
                        this.Close();
                    }
                }
            }
            catch(Exception E)
            {
                MessageBox.Show(E.Message);


            }

         
        }

     
    }
}

